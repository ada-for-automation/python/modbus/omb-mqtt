#!/usr/bin/env python3

"""
 This Python script implements a Modbus TCP Client to MQTT Gateway

 Tutorial :
 http://www.steves-internet-guide.com/mqtt-python-beginners-course/
 
 References :
 https://pypi.org/project/paho-mqtt/
 
 https://pypi.org/project/modbus-tk/

"""

import signal
import struct
import time
import os

import paho.mqtt.client as mqttc

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp, hooks
import logging

MQTT_Connection_Status = \
[
    "Connection successful",                              # 0
    "Connection refused - incorrect protocol version",    # 1
    "Connection refused - invalid client identifier",     # 2
    "Connection refused - server unavailable",            # 3
    "Connection refused - bad username or password",      # 4
    "Connection refused - not authorised",                # 5
    "Currently unused"                                    # 6 - 255
]

running = True

def quit():
    global running
    running = False
    print("Main : User interrupt !")

def sig_handler(signum, frame):
    quit()

def on_log(client, userdata, level, buf):
    print("MQTT Client log :", buf)

def on_connect(client, userdata, flags, rc):
    print("MQTT :", MQTT_Connection_Status[rc])
    
    if rc == 0 :
        print("MQTT : Subscribe to topics")
        client.subscribe(
        [
          ("OMB/Hello", 0),
          ("ps7/s7-315-2dp/mw0", 0)
        ])

        print("MQTT : Say hello !")
        client.publish("OMB/Hello", "True")

def on_disconnect(client, userdata, rc):
    if rc != 0 :
        print("MQTT : Unexpected disconnection.")
    else :
        print("MQTT : Disconnected.")

def on_message(client, userdata, message):
    print("MQTT : Got message :",
          "\n\tpayload\t\t",    str(message.payload.decode("utf-8")),
          "\n\ttopic\t\t",      message.topic,
          "\n\tqos\t\t",        message.qos,
          "\n\tretain flag\t",  message.retain)

    if message.topic == "ps7/s7-315-2dp/mw0" :
        userdata["S7_MW0"] = int(str(message.payload.decode("utf-8")))
        print("MQTT : S7_MW0 = ", userdata["S7_MW0"])


def main():
    """
    Modbus TCP Client to MQTT Gateway Main
    """

    print("=============================================\n"
          "   Modbus TCP Client to MQTT Gateway Main !  \n"
          "=============================================\n")

    signal.signal(signal.SIGINT, sig_handler)

    # MBS stands for Modbus Server
    MBS_Connect_Done = False
    MBS_IP_ADDR = os.environ.get("MBS_IP_ADDR", "192.168.1.100")
    MBS_PORT = 502

    logger = modbus_tk.utils.create_logger("console", level=logging.DEBUG)

    def on_after_recv(data):
        master, bytes_data = data
        logger.info(bytes_data)

    hooks.install_hook('modbus.Master.after_recv', on_after_recv)

    BROKER_HOST = os.environ.get("MQTT_BROKER_HOST", "localhost")

    MyData = {"S7_MW0" : 0}

    print("MQTT : Creating an instance of MQTT Client")
    mqtt_client = mqttc.Client(mqttc.CallbackAPIVersion.VERSION1,
                              "omb", userdata = MyData)

    print("MQTT : Connecting handlers")
    mqtt_client.on_log = on_log
    mqtt_client.on_connect = on_connect
    mqtt_client.on_disconnect = on_disconnect
    mqtt_client.on_message = on_message

    print("MQTT : Connecting to broker :", BROKER_HOST)
    mqtt_client.connect(BROKER_HOST)

    print("MQTT : Starting loop")
    mqtt_client.loop_start()

    print("=========================================\n"
          "   IO Loop : Ctrl-c to terminate !\n"
          "=========================================\n")

    while running:

        if not MBS_Connect_Done:

            print("Main : Connection to Modbus Server {ipaddr}/{port}"
                  .format(ipaddr = MBS_IP_ADDR, port = MBS_PORT))

            try:
                omb_client = modbus_tcp.TcpMaster(host = MBS_IP_ADDR,
                                                  port = MBS_PORT)

            except ConnectionRefusedError:
                logger.error("ConnectionRefusedError")

            except modbus_tk.modbus.ModbusError as exc:
                logger.error("%s- Code=%d", exc, exc.get_exception_code())

            except OSError as exc :
                print("Main : Error Connecting to Modbus Server :\n", exc)

            else:
                MBS_Connect_Done = True

        else:

            while running and MBS_Connect_Done:

                try:
                    my_byte_array = omb_client.execute(1,
                                                       cst.READ_INPUT_REGISTERS,
                                                       0, 4, returns_raw = True)

                    omb_client.execute(1, cst.WRITE_SINGLE_REGISTER, 50,
                                       output_value = MyData["S7_MW0"])

                except ConnectionRefusedError:
                    logger.error("ConnectionRefusedError")
                    MBS_Connect_Done = False

                except modbus_tk.modbus.ModbusError as exc:
                    logger.error("%s- Code=%d", exc, exc.get_exception_code())
                    MBS_Connect_Done = False

                except OSError as exc :
                    print("Main : Error exchanging with Modbus Server :\n",
                          exc)
                    MBS_Connect_Done = False

                else:
                    My_Uint, My_Int = struct.unpack('>Hh', my_byte_array[0:4])
                    My_Real = \
                    struct.unpack('>f', my_byte_array[6:] + my_byte_array[4:6])

                    mqtt_client.publish("OMB/server0/My_Uint", My_Uint)
                    mqtt_client.publish("OMB/server0/My_Int", My_Int)
                    mqtt_client.publish("OMB/server0/My_Real", My_Real[0])

                    print("Main : My_Uint = ", My_Uint,
                          "MyData['S7_MW0'] =", MyData["S7_MW0"])
                    time.sleep(1.05)

    print("MQTT : Say Bye !")
    mqtt_client.publish("OMB/Hello", "False")

    print("MQTT : Unsubscribe from topics")
    mqtt_client.unsubscribe(
    [
      "OMB/Hello",
      "ps7/s7-315-2dp/mw0"
    ])

    print("MQTT : Wait a few seconds...")
    time.sleep(5)

    print("MQTT : Stoping loop")
    mqtt_client.loop_stop()

    print("MQTT : Disconnecting from broker")
    mqtt_client.disconnect()

if __name__ == "__main__":
    main()

