= Modbus TCP to MQTT Gateway
Stéphane LOS
v2024.03, 2024-03-29
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images

== Description

*OMB-MQTT.py*

This Python script implements a Modbus TCP to MQTT Gateway

.Dockerfile
NOTE: A Dockerfile is provided for convenience.

== References

https://pypi.org/project/paho-mqtt/

https://pypi.org/project/modbus-tk/

== Tutorial

http://www.steves-internet-guide.com/mqtt-python-beginners-course/

== Some notes

Create a virtual environment :

----
python3 -m venv ./venv
----

and activate it :

----
source venv/bin/activate
----

Install Modbus TestKit

----
pip install modbus_tk
----

Play in Interpreter with formats

----
python
>>> import modbus_tk
>>> import modbus_tk.defines as cst
>>> from modbus_tk import modbus_tcp, hooks
>>> import os
>>> import struct
>>> MBS_IP_ADDR = os.environ.get("MBS_IP_ADDR", "192.168.1.100")
>>> MBS_PORT = 502
>>> omb_client = modbus_tcp.TcpMaster(host = MBS_IP_ADDR, port = MBS_PORT)
>>> omb_client.execute(1, cst.READ_INPUT_REGISTERS, 0, 1, data_format = '>H')
>>> my_byte_array = omb_client.execute(1, cst.READ_INPUT_REGISTERS, 0, 1, returns_raw = True)
>>> result = struct.unpack('>H', my_byte_array)
>>> my_byte_array = omb_client.execute(1, cst.READ_INPUT_REGISTERS, 0, 4, returns_raw = True)
>>> result = struct.unpack('>Hhf', my_byte_array)
>>> My_Uint, My_Int = struct.unpack('>Hh', my_byte_array[0:4])
>>> result = struct.unpack('<f', my_byte_array[4:])
>>> My_Real = struct.unpack('>f', my_byte_array[6:] + my_byte_array[4:6])
----

Install Eclipse Paho MQTT Python client library

----
pip install paho-mqtt
----

OMB-MQTT.py uses environment variables for Modbus Server connection settings. +
MBS stands for Modbus Server. +
Those settings are available in MBS_IP_ADDR. +
Export if needed for example with :

----
export MBS_IP_ADDR=192.168.1.22
----

OMB-MQTT.py uses environment variable for MQTT Broker connection settings. +
Default is localhost. +
Export if needed for example with :

----
export MQTT_BROKER_HOST=10.252.253.1
----

Run script

----
python OMB-MQTT.py
----

Test with MQTT Client +
Escape $ sign or use single quotes !

----
mosquitto_sub -h localhost -t \$SYS/broker/uptime -v
mosquitto_sub -h localhost -t '$SYS/broker/version' -v

mosquitto_sub -h localhost -t OMB/server0/My_Uint -v

mosquitto_sub -h localhost -t OMB/server0/# -v
----

Freeze Requirements

----
pip freeze > requirements.txt
----

Virtual environment deactivation :

----
deactivate
----

== Docker usage

This paragraph is devoted to the use of the provided Dockerfile on a Hilscher 
netFIELD Device such as the Compact or On Premise ones.

The netFIELD OS offers a Terminal in its Cockpit based web interface.

So open the Terminal, create some folder, get inside and...

=== Get Dockerfile

----
wget "https://gitlab.com/ada-for-automation/python/modbus/omb-mqtt/-/raw/main/Dockerfile?ref_type=heads&inline=false" \
--output-document=Dockerfile
----

=== Build image with

----
docker build --tag=python-omb-mqtt:3.12-slim .
----

=== Run with

----
docker run --detach --name python-omb-mqtt --restart=always \
--env MQTT_BROKER_HOST=10.252.253.1 python-omb-mqtt:3.12-slim
----


