# syntax=docker/dockerfile:1

# Get Dockerfile :
# wget "https://gitlab.com/ada-for-automation/python/modbus/omb-mqtt/-/raw/main/Dockerfile?ref_type=heads&inline=false" \
# --output-document=Dockerfile

# Build image with :
# docker build --tag=python-omb-mqtt:3.12-slim .

# Run with :
# docker run --detach --name python-omb-mqtt --restart=always \
# --env MQTT_BROKER_HOST=10.252.253.1 python-omb-mqtt:3.12-slim

# Use python-slim as base image
FROM python:3.12-slim

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Python / Modbus / MQTT"

# Version
ENV PYTHON_MODBUS_MQTT_VERSION 0.0.1

WORKDIR /app

# Install software
RUN apt-get update && apt-get install -y \
    git \
    && rm -rf /var/lib/apt/lists/*

# Install demo application in work dir
RUN git clone https://gitlab.com/ada-for-automation/python/modbus/omb-mqtt.git .

# Create a virtual environment, activate it and install requirements
RUN python3 -m venv ./venv \
    && . ./venv/bin/activate \
    && pip3 install -r requirements.txt

# Do entrypoint
ENTRYPOINT . ./venv/bin/activate \
           && python OMB-MQTT.py


